const signin = require('./signin').signin;
const signup = require('./signup').signup;
const facebook = require('./facebook');

module.exports = {
  signin,
  signup,
  facebook,
};
