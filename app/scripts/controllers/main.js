'use strict';

/**
 * @ngdoc function
 * @name feedhiveApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the feedhiveApp
 */
angular.module('feedhiveApp')
  .controller('MainCtrl', function ($scope, $routeParams, $http, $route) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.name = window.user.name;
  });
