'use strict';

/**
 * @ngdoc function
 * @name feedhiveApp.controller:SignupCtrl
 * @description
 * # SignupCtrl
 * Controller of the feedhiveApp
 */
angular.module('feedhiveApp')
  .controller('SignupCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.formData = {};
    jQuery.validator.setDefaults({
      errorClass: 'help-block',
      errorElement: 'label',
    });

    $scope.register = function (form) {
      $http.post('/signup', $scope.formData)
      .then(function(res) {
        if (res.statusText === 'OK') {
          console.log(res);
        } else {
          console.log(res);
        }
      });
    };
  }]);
