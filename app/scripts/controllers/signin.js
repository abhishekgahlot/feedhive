'use strict';

/**
 * @ngdoc function
 * @name feedhiveApp.controller:SigninCtrl
 * @description
 * # SigninCtrl
 * Controller of the feedhiveApp
 */
angular.module('feedhiveApp')
  .controller('SigninCtrl', function ($scope, $routeParams, $http, $route) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $(document).keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
        $('#login').click();
      }
    });


    $('#login').on('click', function(){
      $http.post('/signin', {
        'email': $('#email').val(),
        'password': $('#password').val(),
      })
      .then(function(res) {
        if (res.data.auth) {
          window.location.href = 'app';
        } else {
          // show error
        }
      });
    });
  });
