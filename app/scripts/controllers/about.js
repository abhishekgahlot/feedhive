'use strict';

/**
 * @ngdoc function
 * @name feedhiveApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the feedhiveApp
 */
angular.module('feedhiveApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
