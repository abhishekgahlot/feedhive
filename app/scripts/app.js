'use strict';

/**
 * @ngdoc overview
 * @name feedhiveApp
 * @description
 * # feedhiveApp
 *
 * Main module of the application.
 */
angular
  .module('feedhiveApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about',
      })
      .when('/signin', {
        controller: 'SigninCtrl',
        controllerAs: 'signin',
      })
      .when('/signup', {
        controller: 'SignupCtrl',
        controllerAs: 'signup',
      })
      .otherwise({
        redirectTo: '/',
      });
  }).factory('httpInterceptor', function ($q, $rootScope, $log) {

  var numLoadings = 0;

  return {
    request: function (config) {
        numLoadings++;
        $('#loader').removeClass('hidden');
        $rootScope.$broadcast("loader_show");
        return config || $q.when(config)
    },
    response: function (response) {
        if ((--numLoadings) === 0) {
          $('#loader').addClass('hidden')
          $rootScope.$broadcast("loader_hide");
        }
        return response || $q.when(response);
    },
    responseError: function (response) {
      if (!(--numLoadings)) {
        $('#loader').addClass('hidden')
        $rootScope.$broadcast("loader_hide");
      }
      return $q.reject(response);
    }
  };
})
.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}).directive("loader", function ($rootScope) {
  return function ($scope, element, attrs) {
    $scope.$on("loader_show", function () {
        return element.show();
    });
    return $scope.$on("loader_hide", function () {
        return element.hide();
    });
  };
});
