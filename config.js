module.exports = {
  port: 80,
  db: {
    url: 'mongodb://localhost:27017/feedhive',
    index: { email: 1 },
  },
  auth: {
    facebook: {
      id: '1589008021426802',
      secret: '60c81a2b271d6e73e3853f26eefdbb1e',
      callback: 'http://localhost/auth/facebook/callback',
      profileFields: ['id', 'emails', 'name'],
    },
  },
  saltRounds: 11,
};
